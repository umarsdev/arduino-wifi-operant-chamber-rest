//server designed for the Arudion Uno WiFi
//software based on the REST client and server programs found at http://www.arduino.org/learning/tutorials/boards-tutorials/restserver-and-restclient

//Current issues:
// [ ] No response at the appropriate web addresses.
//      Possibly related to server/arduino/
/*available commands are: 
 * digital read write
 * analog read
 * write and time response
 */

// http://192.168.0.1/arduino/writeAndTime/outPin/value/inPin/value/timeoutInms

#include <Wire.h>
#include <ArduinoWiFi.h>
//#include <UnoWiFiDevEd.h>

void setup() {
  Wifi.begin();
  Wifi.println("Rest Server is up");

  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  for(int i = 5; i <=9; i++){
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }
  for(int i = A0; i<= A5; i++){
    pinMode(i, INPUT);
  }
  Serial.begin(9600);
  while(!Serial)
  {
  }
  Serial.println("all setup");

}

void loop() {
  while(Wifi.available());
  {
      //Serial.print(F("got a command at"));
      //Serial.println(millis());
      //Wifi.println("Got a command over wifi");
      process(Wifi);
  }
  delay(50); //this is in the example, and is only 50ms, so we will leave it for now
}

void process(WifiData client){
  //get command
  Serial.println(Wifi.available());
  String command = client.readStringUntil('/');
  Serial.println(command);
  Wifi.print('p');
  if (command == "digital"){
    Wifi.print('d'); //log that we got a digital command
    digitalCommand(client);
  }else if(command == "analog"){
    analogCommand(client);
  }else if(command == "writeAndTime"){
    writeAndTime(client);
  }
}

void digitalCommand(WifiData client){
  int pin, value;
  pin = client.parseInt(); // gets the next int value
  if(client.read() == '/'){ //if we have more data (a specified output value)
    value = client.parseInt();
    digitalWrite(pin, value);
  }else{
    value = digitalRead(pin);
  }
  //send the response
  client.println("Status: 200 OK\n");
  client.print(F("Pin: "));
  client.print(pin);
  client.print(F(" set to "));
  client.println(value);
  client.print(EOL);    //char terminator
}

void analogCommand(WifiData client){
  //currently includes reading values only
  int pin, value;
  pin = client.parseInt();
  value = analogRead(pin);
  //display value
  client.println("Status: 200 OK\n");
  client.print(F("Pin A"));
  client.print(pin);
  client.print(F(" reads analog "));
  client.println(value);
  client.print(EOL); //char terminator
}

void writeAndTime(WifiData client){
  //format is /WritePin/Value/ReadPin/Value/Timeout in ms
  int outPin, outValue, inPin, inValue, errorValue;
  unsigned long startTime, totalTime, timeout;
  outPin = client.parseInt();
  if(outPin > 9 || outPin < 5){
    errorValue = 4; //4: pin value out of range
    goto ERROR;
  }
  //outValue
  if(client.read() == '/'){
    outValue = client.parseInt();
  }else{
    errorValue = 1; // 1: not enough inputs
    goto ERROR;
  }
  //inPin
  if(client.read() == '/'){
    inPin = client.parseInt();
  }else{
    errorValue = 1; // 1: not enough inputs
    goto ERROR;
  }
   if(inPin > A5 || outPin < A0 ){
    errorValue = 4; //4: pin value out of range
    goto ERROR;
  }
  //inValue
  if(client.read() == '/'){
    inValue = client.parseInt();
  }else{
    errorValue = 1; // 1: not enough inputs
    goto ERROR;
  }
  //timeout
  if(client.read() == '/'){
    timeout = client.parseInt();
  }else{
    timeout = 0; // default to no timeout
  }
  
  //begin processing sequence
  digitalWrite(outPin, outValue);
  startTime = millis();
  if(digitalRead(inPin) == inValue){ //already trigered
    errorValue = 3;
    goto ERROR;
  }
  while(digitalRead(inPin) != inValue){
    if(timeout){//only process the timeout stuff if we are checking for a timeout
      unsigned long currentTime = millis();
      if(currentTime-startTime >= timeout){
        errorValue = 2;
        goto ERROR;
      }
    }
  }
  //this means that inPin==inValue;
  totalTime = millis()-startTime;
  //display error status
  client.println("Status: 200 OK\n");
  //client.print(F("Response time: "));
  client.println(totalTime);
  client.print(EOL);    //char terminator
  return;//exit if no error
  
  
  ERROR:
  //1: not enough inputs
  //2: timeout
  //3: inPin triggered at startup
  //4: pin out of range
  //display error status
  digitalWrite(13, HIGH);
  switch(errorValue){
    case 1:
      client.println("Status: 400\n");
      client.println("not enough inputs");

  
    break;
  }
  client.print(EOL);    //char terminator for all errors
  
}



